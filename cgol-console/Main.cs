// 
// Main.cs
//  
// Author:
//       Jan Henrik Hasselberg <jhh@f-m.fm>
// 
// Copyright (c) 2013 Jan Henrik Hasselberg
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;
using cgollib;

namespace cgolconsole
{
	class MainClass
	{
		
		public static CellDish StartNewGame (int rows, int columns)
		{
			var game = new CellDish (rows, columns);
			Random rand = new Random ();
			int loop = rand.Next ((rows + columns) / 2, rows * columns / 8);
			do {
				game.ActivateCell (rand.Next (0, rows - 1), rand.Next (0, columns - 1));
			} while(loop-- > 0);
			return game;
		}
		
		public static void Main (string[] args)
		{
			int rows = 20;
			int columns = 50;
			CellDish game = StartNewGame (rows, columns);
			ConsoleKeyInfo cki;
			int generation = 0;
			do {
				Console.WriteLine ("Press ESC to quit, ENTER for restart, another key to continue.");
				Console.WriteLine (string.Format ("Generation {0}.\n", generation++));
				for (int i = 0; i < rows; i++) {
					for (int j = 0; j < columns; j++) {
						if (game.IsCellAlive (i, j)) {
							Console.Write ("0");
						} else {
							Console.Write (" ");
						}
						game.NewGeneration (i, j);
					}
					Console.WriteLine ();
				}
				cki = Console.ReadKey (true);
				if (cki.Key == ConsoleKey.Enter) {
					game = StartNewGame (rows, columns);
					generation = 0;
				}
				Console.Clear ();
			} while(cki.Key != ConsoleKey.Escape);
		}
	}
}

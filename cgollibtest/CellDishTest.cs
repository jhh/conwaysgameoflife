// 
// CellDishTest.cs
//  
// Author:
//       Jan Henrik Hasselberg <jhh@f-m.fm>
// 
// Copyright (c) 2013 Jan Henrik Hasselberg
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using NUnit.Framework;
using cgollib;

namespace cgollibtest
{
	public class CellDishTest
	{
		private CellDish _cd;
		private const int _cellRows = 50;
		private const int _cellColumns = 50;
		
		[SetUp]
		public void Initialize ()
		{
			_cd = new CellDish (_cellRows, _cellColumns);
		}
		
		[Test]
		public void IsCellAliveEqualFalseTest ()
		{
			Assert.IsFalse (_cd.IsCellAlive (2, 2));
		}
		
		[Test]
		[ExpectedException("System.IndexOutOfRangeException")]
		public void AssertArgumentExceptionIfTestForAliveCellOutsideDishTest ()
		{
			_cd.IsCellAlive (2, _cellColumns);
		}
		
		[Test]
		public void CellCoordinateIsWithinDishTest ()
		{
			Assert.IsTrue (_cd.CoordinateIsWitinDish (_cellRows - 1, _cellColumns - 1));
			Assert.IsTrue (_cd.CoordinateIsWitinDish (0, 0));
			Assert.IsTrue (_cd.CoordinateIsWitinDish (0, _cellColumns - 1));
			Assert.IsTrue (_cd.CoordinateIsWitinDish (_cellRows - 1, 0));
		}
		
		[Test]
		[ExpectedException("System.IndexOutOfRangeException")]
		public void CellCoordinateIsNotWithinDishTest1 ()
		{
			_cd.CoordinateIsWitinDish (_cellRows, _cellColumns);
		}
		
		[Test]
		[ExpectedException("System.IndexOutOfRangeException")]
		public void CellCoordinateIsNotWithinDishTest2 ()
		{
			_cd.CoordinateIsWitinDish (-1, -1);
		}
		
		[Test]
		[ExpectedException("System.IndexOutOfRangeException")]
		public void CellCoordinateIsNotWithinDishTest3 ()
		{
			_cd.CoordinateIsWitinDish (-1, _cellColumns);
		}
		
		[Test]
		[ExpectedException("System.IndexOutOfRangeException")]
		public void CellCoordinateIsNotWithinDishTest4 ()
		{
			_cd.CoordinateIsWitinDish (_cellRows, -1);
		}
		
		[Test]
		public void SetCellAliveTest ()
		{
			_cd.ActivateCell (5, 5);
			Assert.IsTrue (_cd.IsCellAlive (5, 5));
		}
		
		[Test]
		public void SetCellDeadTest ()
		{
			_cd.ActivateCell (5, 5);
			_cd.KillCell (5, 5);
			Assert.IsFalse (_cd.IsCellAlive (5, 5));
		}
		
		[Test]
		public void LiveCellHasNoNeighborsTest ()
		{
			_cd.ActivateCell (5, 5);
			Assert.AreEqual (0, _cd.CountNeighbors (5, 5));
		}
		
		[Test]
		public void LiveCellHasOneNeighborsTest ()
		{
			_cd.ActivateCell (5, 5);
			_cd.ActivateCell (5, 6);
			Assert.AreEqual (1, _cd.CountNeighbors (5, 5));
		}
		
		[Test]
		[ExpectedException("System.IndexOutOfRangeException")]
		public void CountNeighborOutsideDishThrowsException ()
		{
			_cd.CountNeighbors (_cellRows, _cellColumns);
		}
		
		[Test]
		public void CellOnBorderOfDishHaveNeighborsOnOppositeSideOfDishTest ()
		{
			_cd._dish [5, _cellColumns - 1] = _cd._dish [5, 0] = _cd._dish [_cellRows - 1, 10] = _cd._dish [0, 10] = _cd._dish [0, 11] = _cd._dish [0, 0] = _cd._dish [_cellRows - 1, _cellColumns - 1] = CellDish.Cell.Alive;
			Assert.AreEqual (1, _cd.CountNeighbors (5, _cellColumns - 1), "Cell at 5x49 expect to find neighbor at 5x0.");
			Assert.AreEqual (2, _cd.CountNeighbors (_cellRows - 1, 10), "Cell at 49x10 expect to find neighbor at 0x10 and 0x11.");
			Assert.AreEqual (1, _cd.CountNeighbors (0, 0), "Cell at 0x0 expect to find neighbor at 49x49.");
		}
		
		//  Any live cell with fewer than two live neighbors dies, 
		//  as if caused by under-population.
		
		[Test]
		public void CellWithZeroNeighborsDieTest ()
		{
			_cd.ActivateCell (5, 5);
			_cd.NewGeneration (5, 5);
			Assert.IsFalse (_cd.IsCellAlive (5, 5));
		}
		
		[Test]
		public void CellWithOneNeighborsDieTest ()
		{
			_cd.ActivateCell (5, 5);
			_cd.ActivateCell (4, 4);
			_cd.NewGeneration (5, 5);
			Assert.IsFalse (_cd.IsCellAlive (5, 5));
		}
		
		//  Any live cell with two or three live neighbors lives on to the 
		//  next generation.
		
		[Test]
		public void CellWithTwoNeighborsSurviveTest ()
		{
			_cd.ActivateCell (5, 5);
			_cd.ActivateCell (4, 4);
			_cd.ActivateCell (6, 6);
			_cd.NewGeneration (5, 5);
			Assert.IsTrue (_cd.IsCellAlive (5, 5));
		}
		
		[Test]
		public void CellWithTreeNeighborsSurviveTest ()
		{
			_cd.ActivateCell (5, 5); // Cell.
			_cd.ActivateCell (4, 4); // Neighbor 1
			_cd.ActivateCell (6, 6); // Neighbor 2
			_cd.ActivateCell (5, 6); // Neighbor 3
			_cd.NewGeneration (5, 5);
			Assert.IsTrue (_cd.IsCellAlive (5, 5));
		}
		
		//  Any live cell with more than three live neighbors dies, 
		//  as if by overcrowding.
		
		[Test]
		public void CellWithFourNeighborsDieTest ()
		{
			_cd.ActivateCell (5, 5); // Cell.
			_cd.ActivateCell (4, 4); // Neighbor 1
			_cd.ActivateCell (6, 6); // Neighbor 2
			_cd.ActivateCell (5, 6); // Neighbor 3
			_cd.ActivateCell (4, 6); // Neighbor 4
			_cd.NewGeneration (5, 5);
			Assert.IsFalse (_cd.IsCellAlive (5, 5));
		}
		
		[Test]
		public void CellWithFiveToEightNeighborsDieTest ()
		{
			int row = 5;
			int column = 5;
			string errorMsg = "Assert cell at {0}x{1} die in new generation with {2} neighbors.";
			CellWithFiveToEightNeighborsHelper (row, column, errorMsg);
		}
		
		[Test]
		public void CellAtEdgeOfDishWithFiveToEightNeighborsDieTest1 ()
		{
			int row = 0;
			int column = 0;
			string errorMsg = "Assert cell at edge of dish; {0}x{1}, die in new generation with {2} neighbors.";
			CellWithFiveToEightNeighborsHelper (row, column, errorMsg);
		}

		[Test]
		public void CellAtEdgeOfDishWithFiveToEightNeighborsDieTest2 ()
		{
			int row = _cellRows - 1;
			int column = _cellColumns - 1;
			string errorMsg = "Assert cell at edge of dish; {0}x{1}, die in new generation with {2} neighbors.";
			CellWithFiveToEightNeighborsHelper (row, column, errorMsg);
		}
		
		private void CellWithFiveToEightNeighborsHelper (int row, int column, string errorMsg)
		{
			int testRan = 0;
			for (int i = row-1; i <= row + 1; i++) {
				for (int j = column-1; j <= column + 1; j++) {
					_cd.ActivateCell (_cd.FindOppositeRow (i), _cd.FindOppositeColumn (j)); // Neighbor
					_cd.ActivateCell (row, column); // Cell.
					int countNeighbors = _cd.CountNeighbors (row, column);
					if (!(i == row && j == column) && countNeighbors > 4) {
						_cd.NewGeneration (row, column);
						Assert.IsFalse (_cd.IsCellAlive (row, column), string.Format (errorMsg, row, column, countNeighbors));
						testRan++;
					}
				}
				
			}
			
			if (testRan != 4) {
				throw new Exception ("There is an logical error in test code.");
			}
		}
		
		//  Any dead cell with exactly three live neighbors becomes a live cell, 
		//  as if by reproduction.
		
		[Test]
		public void DeadCellWithThreeLiveNeighborsBecomeALiveCellTest ()
		{
			_cd.ActivateCell (4, 4); // Neighbor 1
			_cd.ActivateCell (6, 6); // Neighbor 2
			_cd.ActivateCell (5, 6); // Neighbor 3
			_cd.NewGeneration (5, 5);
			Assert.IsTrue (_cd.IsCellAlive (5, 5));
		}
	}
}


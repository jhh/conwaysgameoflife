// 
// CellDish.cs
//  
// Author:
//       Jan Henrik Hasselberg <jhh@f-m.fm>
// 
// Copyright (c) 2013 Jan Henrik Hasselberg
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;

namespace cgollib
{
	public class CellDish
	{
		internal enum Cell
		{
			Dead,
			Alive
		}
		internal Cell[,] _dish;
		
		public CellDish (int dishRows, int dishColumns)
		{
			_dish = new Cell[dishRows, dishColumns];
		}

		public bool CoordinateIsWitinDish (int row, int column)
		{
			if (row < _dish.GetLength (0) && row >= 0 || column < _dish.GetLength (1) && column >= 0) {
				return true;
			}
			throw new IndexOutOfRangeException ("Row or column is out of scoope.");
		}

		public bool IsCellAlive (int row, int column)
		{
			if (CoordinateIsWitinDish (row, column) && _dish [row, column] == Cell.Alive) {
				return true;
			}
			return false;
		}

		public void ActivateCell (int row, int column)
		{
			if (CoordinateIsWitinDish (row, column)) {
				_dish [row, column] = Cell.Alive;
			}
		}
		
		public void KillCell (int row, int column)
		{
			if (CoordinateIsWitinDish (row, column)) {
				_dish [row, column] = Cell.Dead;
			}
		}

		public int CountNeighbors (int row, int column)
		{
			int count = IsCellAlive (row, column) ? -1 : 0;
			for (int i = row - 1; i <= row + 1; i++) {
				for (int j = column - 1; j <= column + 1; j++) {
					count += IncrementNeighborCounter (i, j);
				}
			}
			return count;
		}
		
		internal int IncrementNeighborCounter (int row, int column)
		{
			try {
				if (IsCellAlive (row, column)) {
					return 1;
				}
			} catch (IndexOutOfRangeException) {
				int newRow = FindOppositeRow (row);
				int newColumn = FindOppositeColumn (column);
				if (IsCellAlive (newRow, newColumn)) {
					return 1;
				} else {
					return 0;
				}
			}
			return 0;
		}

		internal int FindOppositeRow (int row)
		{
			if (row >= 0 && row < _dish.GetLength (0)) {
				return row;
			} else if (row == -1) {
				return _dish.GetLength (0) - 1;
			} else if (row == _dish.GetLength (0)) {
				return 0;
			}
			throw new FormatException (string.Format ("Expected value from {0} to {1}", -1, _dish.GetLength (0)));
		}
		
		internal int FindOppositeColumn (int column)
		{
			if (column >= 0 && column < _dish.GetLength (1)) {
				return column;
			} else if (column == -1) {
				return _dish.GetLength (1) - 1;
			} else if (column == _dish.GetLength (1)) {
				return 0;
			}
			throw new FormatException (string.Format ("Expected value from {0} to {1}", -1, _dish.GetLength (1)));
		}

		public void NewGeneration (int row, int column)
		{
			int neighbors = CountNeighbors (row, column);
			bool cellIsAlive = IsCellAlive(row, column);
			
			//  Any live cell with fewer than two live neighbors dies, 
			//  as if caused by under-population.
			if (cellIsAlive && neighbors < 2) {
				KillCell(row, column);
			}
			
			//  Any live cell with more than three live neighbors dies, 
			//  as if by overcrowding.
			else if (cellIsAlive && neighbors > 3) {
				KillCell(row, column);
			}
			
			//  Any dead cell with exactly three live neighbors becomes a live cell, 
			//  as if by reproduction.
			else if (!cellIsAlive && neighbors == 3) {
				ActivateCell(row, column);
			}
		}
	}
}

